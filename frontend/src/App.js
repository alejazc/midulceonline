import React, { Fragment } from 'react';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Login from './pages/auth/Login';

import Navigation from "./components/Navigation";
import productList from './components/productList'
import createProduct from "./components/createProduct";
import createClient from "./components/createClient";
import Register from './pages/auth/Register';
import Home from './pages/Home';
import Hook from './pages/auth/Hook';



function App() {
  return (
    <Fragment>
      <Router>
        <Routes>
          <Route path='/' exact element={ <Home/> } />
          <Route path='/login' exact element={ <Login/> } />
          <Route path='/register' exact element={ <Register/> } />
          <Route path='/hook' exact element={ <Hook/> } />
          </Routes>
      </Router>
    </Fragment>
  );
}

export default App;
