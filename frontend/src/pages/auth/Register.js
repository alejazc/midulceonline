import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import APIInvoke from '../../utils/APIInvoke';
import swal from 'sweetalert';

const Register = () => {

    const [ user, setUser ] = useState(
        {
            name: '',
            email: '',
            contraseña: '',
            confirm: ''
        }
    );

    const { name, email, contraseña, confirm } = user;

    const onChange = (event) => {
        setUser(
            {
                ...user, 
                [ event.target.name ] : event.target.value
            }
        );
    }

    const saveUser = async () => {

        if( contraseña !== confirm ){
            swal({
                title: 'Error',
                icon: 'error',
                text: 'Contraseña debe coincidir',
                buttons:{
                    confirm:{
                    text: 'Ok',
                    value: true,
                    visible: true,
                    className: 'btn btn-danger',
                    closeModal: true
                    }
                }
            });
        }else{
            const body = {
                name: user.name,
                email: user.email,
                contraseña: user.contraseña 
            }

            const response = await APIInvoke.invokePOST(`/cliente/crear`, body );
            
            if( response.message === 'Registro existente'){
                swal({
                    title: 'Usuario ya creado',
                    icon: 'warning',
                    text: 'Usuario ya existente',
                    buttons:{
                        confirm:{
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-warning',
                        closeModal: true
                        }
                    }
                });

            }else{
                swal({
                    title: 'Éxito',
                    icon: 'success',
                    text: 'Usuario registrado',
                    buttons:{
                        confirm:{
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-success',
                        closeModal: true
                        }
                    }
                });
            }
            
        }

    }

    useEffect( () => {
        document.getElementById("name").focus();
    } , []);

    const onSubmit = (event) => {
        event.preventDefault();
        saveUser();
    }

    return (
        <div className="hold-transition register-page" style={{alignItems: 'center',}}>
            <div className="login-box">
                <div className="card card-outline card-primary">
                    <div className="card-header text-center">
                        <a href="../../index2.html" className="h1"><b>MiDulce</b>Online</a>
                    </div>
                    <div className="card-body">
                        <p className="login-box-msg">Registrar nuevo usuario</p>
                        
                        
                        <form onSubmit={ onSubmit }>
                            <div className="input-group mb-3">
                                <input type="text" className="form-control" placeholder="Nombre completo"
                                id="name"
                                name="name" 
                                value={name} 
                                onChange={ onChange }
                                required/>
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-user" />
                                    </div>
                                </div>
                            </div>
                            <div className="input-group mb-3">
                                <input type="email" className="form-control" placeholder="Email"
                                id="email"
                                name="email" 
                                value={email} 
                                onChange={ onChange }
                                required/>
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-envelope" />
                                    </div>
                                </div>
                            </div>
                            <div className="input-group mb-3">
                                <input type="password" className="form-control" placeholder="Contraseña"
                                id="contraseña"
                                name="contraseña" 
                                value={contraseña} 
                                onChange={ onChange }
                                required/>
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-lock" />
                                    </div>
                            </div>
                        </div>
                        <div className="input-group mb-3">
                            <input type="password" className="form-control" placeholder="Confirme contraseña"
                            id="confirm"
                            name="confirm" 
                            value={confirm} 
                            onChange={ onChange }
                            required/>
                            <div className="input-group-append">
                                <div className="input-group-text">
                                    <span className="fas fa-lock" />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-8">
                                <div className="icheck-primary">
                                    <input type="checkbox" id="agreeTerms" name="terms" defaultValue="agree" />
                                </div>
                            </div>
                            <div className="col-4">
                                <button type="submit" className="btn btn-primary btn-block">Registrarse</button>
                                <br>
                                </br>
                                <Link to="/" className="btn btn-primary btn-danger">Regresar</Link>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Register;
